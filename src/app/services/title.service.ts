import {Injectable} from '@angular/core'
import {Observable} from "rxjs"
import {HttpClient, HttpHeaders} from "@angular/common/http"
import {apiKey, baseUrl, host} from "../../environments/config"

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  constructor(private http: HttpClient) {}

  headers = new HttpHeaders()
    .set('x-rapidapi-key', apiKey)
    .set('x-rapidapi-host', host)

  findAsync(name: string): Observable<FindResponse> {
    return this.http.get<FindResponse>(
      `${baseUrl}/title/find`,
      {
        headers: this.headers,
        params: {['q']: name}
      })
  }
}

export interface FindResponse extends Response {
  results: Film[]
}

interface Film {
  id: number
  image: Image
  runningTimeInMinutes: number
  title: string
  titleType: string
  years: number
  principals: Principal[]
}

export interface Image {
  height: number
  width: number
  id: number
  url: string
}

export interface Principal {
  id: string
  legacyNameText: string
  name: string
  billing: string
  category: string
  characters: string[]
  roles: Role[]
}

export interface Role {
  character: string
  characterId: string
}

export interface Response {
  ['@meta']: Meta
  ['@type']: string
  query: string
  type: string[]
}

export interface Meta {
  operation: string
  requestId: string
  serviceTimeMs: number
}
