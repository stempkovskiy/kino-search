import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {TitleService} from "../../services/title.service";

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchForm = new FormGroup({
    filmName: new FormControl('')
  })

  constructor(private titleService: TitleService) { }

  ngOnInit(): void {}

  getFilm() {
    const name = this.searchForm.get('filmName').value
    if (!name) {
      console.log('not send request...')
      return
    }
    this.titleService.findAsync(name)
      .subscribe(response => console.log(response.results))

  }

}
