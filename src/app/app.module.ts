import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { AppComponent } from './app.component'
import { NewsComponent } from './pages/news/news.component'
import { FooterComponent } from './components/footer/footer.component'
import { CardFilmComponent } from './components/card-film/card-film.component'
import { SearchComponent } from './components/search/search.component'
import { HomeComponent } from './pages/home/home.component'
import {FormsModule, ReactiveFormsModule} from "@angular/forms"
import {AppRoutingModule} from "./app.routing.module"
import {HttpClientModule} from "@angular/common/http"

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    NewsComponent,
    FooterComponent,
    CardFilmComponent,
    SearchComponent,
    HomeComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
